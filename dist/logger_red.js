"use strict";
var LoggerRed = (function () {
    function LoggerRed() {
    }
    LoggerRed.logInfo = function (argument) {
        console.log("\x1b[31m", argument);
        return argument;
    };
    return LoggerRed;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LoggerRed;
