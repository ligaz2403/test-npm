"use strict";
var LoggerYellow = (function () {
    function LoggerYellow() {
    }
    LoggerYellow.logInfo = function (argument) {
        console.log('\x1b[33m%s\x1b[0m', argument);
        return argument;
    };
    return LoggerYellow;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LoggerYellow;
