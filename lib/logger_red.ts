
class LoggerRed {

  static logInfo(argument: string): string {
    console.log("\x1b[31m", argument);

    return argument;
  }
}

export default LoggerRed