
class LoggerYellow {

  static logInfo(argument: string): string {
    console.log('\x1b[33m%s\x1b[0m', argument);

    return argument;
  }
}

export default LoggerYellow