import { expect } from 'chai';
import 'mocha';

import { LoggerYellow, LoggerRed } from '../lib/';

describe('Test logger functions', () => {

  it('should return argument and yellow log', () => {
    const argument = 'test';
    const result = LoggerYellow.logInfo(argument);
    expect(result).to.equal(argument);
  });

  it('should return argument and red log', () => {
    const argument = 'test';
    const result = LoggerRed.logInfo(argument);
    expect(result).to.equal(argument);
  });

});